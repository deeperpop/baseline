from pydub import AudioSegment
from os import listdir
import random

class Baseline:
    def __init__(self, numSongs, seed = None):
        self.songs = Baseline.initializeSongs(numSongs, seed)

    @staticmethod
    def initializeSongs(numSongs, seed):
        """
        Returns a list of numSongs AudioSegments chosen randomley from the
        data subdirectory. Seed is random seed used
        """
        songNames = listdir("data")
        songs = []
        random.seed(seed)

        if numSongs > len(songNames):
            raise InputError("Cannot have more than {} songs".format(len(songNames)))

        chosenSongNames = random.sample(songNames, numSongs)

        for name in chosenSongNames:
            song = AudioSegment.from_mp3("data/" + name)
            songs.append(song)

        return songs


    def generate(self, length=30, seed=None):
        """
        length: length in seconds of song
        seed: random seed
        """
        random.seed(seed)
        result = None

        # Works with 1-second song segments (approximately)

        for _ in xrange(length):
            # Choose random song
            song = random.choice(self.songs)

            # Choose random 1 second segement from song
            songLength = len(song) / 1000 # In seconds
            startIndex = random.randint(1, songLength - 1) * 1000
            endIndex = startIndex + 1000
            #Note: this will only take chunks on the second mark, could improve
            segment = song[startIndex:endIndex]

            if result == None:
                result = segment
            else:
                result += segment

        result.export("random.mp3", format="mp3")

def main():
    baseline = Baseline(2, seed=42)
    baseline.generate(seed=42)


if __name__ == "__main__":
    main()